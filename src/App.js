import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter, Route } from "react-router-dom";

import store from "stateManager/store";
import Main from "components/Main";
import Map from "components/Map";
import "assets/base.scss";

const App = () => {
	return (
		<Provider store={store}>
			<BrowserRouter>
				<Route path="/" exact component={Main} />
				<Route path="/map" component={Map} />
			</BrowserRouter>
		</Provider>
	);
};

export default App;
