import React from "react";
import { ClipLoader } from "react-spinners";

const Loader = () => {
	return (
		<div className="loader_wrapper">
			<ClipLoader size={50} />
		</div>
	);
};

export default Loader;
