import React from "react";
import { useDispatch } from "react-redux";

import { setCurrentPost, removeUserPost } from "stateManager/posts/actions";

const PostCard = ({ post }) => {
	const dispatch = useDispatch();

	const handleClick = (e) => {
		e.stopPropagation();
		dispatch(setCurrentPost(post.userId, post.id));
	};

	const handleDeleteClick = (e) => {
		e.stopPropagation();
		dispatch(removeUserPost(post));
	};

	return (
		<div className="post-card" onClick={handleClick}>
			<div className="post-card_header">
				<div className="title">{post.title}</div>
				<div className="action" onClick={handleDeleteClick}>
					&#215;
				</div>
			</div>
			<div className="post-card_body">{post.body}</div>
		</div>
	);
};

export default PostCard;
