import React, { useState, useEffect, useRef } from "react";
import { useDispatch } from "react-redux";

import { removeCurrentPost, editUserPost } from "stateManager/posts/actions";

const PostForm = ({ post }) => {
	const [formValues, setformValues] = useState({ title: "", body: "" });
	const dispatch = useDispatch();
	const titleButtonRef = useRef(null);
	const submitButtonRef = useRef(null);
	const bodyInputRef = useRef(null);
	const disabled =
		(!formValues.title.trim() || !formValues.body.trim()) && "disabled";

	useEffect(() => {
		setformValues({
			title: post.title,
			body: post.body,
		});
	}, [post]);

	const handleChange = (e) => {
		const { name, value } = e.target;
		setformValues({ ...formValues, [name]: value });
	};

	const closeModal = (e) => {
		dispatch(removeCurrentPost());
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		const new_post = {
			...post,
			title: formValues.title,
			body: formValues.body,
		};

		dispatch(editUserPost(new_post));
		closeModal();
	};

	const onKeyPress = (e) => {
		if (e.key === "Enter") {
			if (e.target.name === "title") {
				bodyInputRef.current.focus();
			} else {
				if (!disabled) {
					bodyInputRef.current.blur();
					submitButtonRef.current.click();
				} else {
					titleButtonRef.current.focus();
				}
			}
		}
	};

	return (
		<div className="backdrop">
			<div className="post_form-card">
				<div className="form-title">edit post</div>
				<div className="form-label">title</div>
				<input
					autoFocus
					className="form-input"
					value={formValues.title}
					name="title"
					onKeyPress={onKeyPress}
					ref={titleButtonRef}
					onChange={handleChange}
				></input>
				<div className="form-label">body</div>
				<textarea
					className="form-input"
					rows="5"
					value={formValues.body}
					name="body"
					onKeyPress={onKeyPress}
					ref={bodyInputRef}
					onChange={handleChange}
				></textarea>
				<div className="form-btns">
					<button
						onClick={handleSubmit}
						ref={submitButtonRef}
						className={`form-btn submit ${disabled}`}
						disabled={disabled}
					>
						confirm
					</button>
					<button onClick={closeModal} className="form-btn cancel">
						cancel
					</button>
				</div>
			</div>
		</div>
	);
};

export default PostForm;
