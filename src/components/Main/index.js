import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import User from "components/User";
import UserPosts from "components/UserPosts";
import PostForm from "components/Post/PostForm";
import Loader from "components/Loader";
import { getUsers } from "stateManager/users/actions";

const Main = () => {
	const dispatch = useDispatch();
	const { users, currentUser, fetching } = useSelector((state) => state.users);
	const currentPost = useSelector((state) => state.posts.currentPost);

	useEffect(() => {
		const fetchUsers = async () => {
			await dispatch(getUsers());
		};

		!users && fetchUsers();
	}, [dispatch, users]);

	return (
		<>
			{!fetching ? (
				users?.length > 0 ? (
					<div className="users_wrapper">
						{users.map((user) => (
							<User
								key={user.id}
								user={user}
								isCurrent={user.id === currentUser?.id}
							/>
						))}
					</div>
				) : (
					<div className="no_users">No users available</div>
				)
			) : (
				<Loader />
			)}
			{currentUser && <UserPosts user={currentUser} />}
			{currentPost && <PostForm post={currentPost} />}
		</>
	);
};

export default Main;
