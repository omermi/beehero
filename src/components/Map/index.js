import React from "react";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import { Redirect } from "react-router-dom";

const Map = (props) => {
	const lat = new URLSearchParams(props.location.search).get("lat");
	const lng = new URLSearchParams(props.location.search).get("lng");

	const position = [lat, lng];
	return lat && lng ? (
		<>
			<div className="map_wrapper">
				<MapContainer center={position} zoom={5} scrollWheelZoom={false}>
					<TileLayer
						attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
						url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
					/>
					<Marker position={position}>
						<Popup>{`${lat}, ${lng}`}</Popup>
					</Marker>
				</MapContainer>
			</div>
		</>
	) : (
		<Redirect to="/" />
	);
};

export default Map;
