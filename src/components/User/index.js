import React from "react";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import { setCurrentUser, removeUser } from "stateManager/users/actions";
import { getPosts } from "stateManager/posts/actions";

const User = ({ user, isCurrent }) => {
	const dispatch = useDispatch();
	const history = useHistory();
	const { posts } = useSelector((state) => state.posts);
	const current = isCurrent && "current";

	const handleClick = (e) => {
		e.stopPropagation();
		posts?.[user.id]
			? dispatch(setCurrentUser(user.id))
			: dispatch(getPosts(user.id));
	};

	const handleDeleteClick = (e) => {
		e.stopPropagation();
		dispatch(removeUser(user.id));
	};

	const handleCoordsClick = (e) => {
		e.stopPropagation();
		history.push(`/map?lat=${user.coords.lat}&lng=${user.coords.lng}`);
	};

	return (
		<div className={`user-card ${current}`} onClick={handleClick}>
			<div className="user-card_header">
				<div className="title_wrapper">
					<div className="title">{user.name}</div>
					<div className="title_sub">({user.username})</div>
				</div>
				<div className="action" onClick={handleDeleteClick}>
					&#215;
				</div>
			</div>
			<div className="user-card_body">
				<div className="item">
					<div className="title">email</div>
					<span className="content">{user.email}</span>
				</div>
				<div className="item">
					<div className="title">company</div>
					<span className="content">{user.company_name}</span>
				</div>
				<div className="item">
					<div className="title">coordinates</div>
					<span
						className="content coords"
						onClick={handleCoordsClick}
					>{`${user.coords.lat}, ${user.coords.lng}`}</span>
				</div>
			</div>
		</div>
	);
};

export default User;
