import React from "react";
import { useSelector } from "react-redux";

import PostCard from "components/Post/PostCard";
import Loader from "components/Loader";

const UserPosts = ({ user }) => {
	const { posts, fetching } = useSelector((state) => state.posts);
	return (
		<div className="user_posts-card">
			<div className="user_posts-card_header">{user.name} posts</div>
			{!fetching ? (
				posts?.[user.id]?.length > 0 ? (
					<div className="user_posts-card_body">
						{posts[user.id].map((post) => (
							<PostCard key={post.id} post={post} />
						))}
					</div>
				) : (
					<div className="no_posts">No posts available</div>
				)
			) : (
				<Loader />
			)}
		</div>
	);
};

export default UserPosts;
