import { api } from "api/config";
import { postsConstants as pc } from "./constants";
import { setCurrentUser } from "stateManager/users/actions";

export const getPosts = (user_id) => async (dispatch) => {
	try {
		dispatch(fetchingPosts(true));
		const res = await api.get(`posts`, { params: { userId: user_id } });
		dispatch(setPosts(user_id, res.data));
		dispatch(setCurrentUser(user_id));
		dispatch(fetchingPosts(false));
	} catch (e) {
		dispatch(fetchingPosts(false));
	}
};

const setPosts = (user_id, posts) => ({
	type: pc.SET_POSTS,
	user_id,
	posts,
});

const fetchingPosts = (boolean) => ({
	type: pc.FETCHING_POSTS,
	boolean,
});

export const setCurrentPost = (user_id, post_id) => ({
	type: pc.SET_CURRENT_POST,
	post_id,
	user_id,
});

export const editUserPost = (post) => ({
	type: pc.EDIT_USER_POST,
	post,
});

export const removeUserPost = (post) => ({
	type: pc.REMOVE_USER_POST,
	post,
});

export const removeUserPosts = (post) => ({
	type: pc.REMOVE_USER_POSTS,
	post,
});

export const removeCurrentPost = () => ({
	type: pc.REMOVE_CURRENT_POST,
});
