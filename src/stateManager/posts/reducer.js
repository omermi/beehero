import { postsConstants as pc } from "./constants";

const initialState = { posts: null, currentPost: null, fetching: false };

export const posts = (state = initialState, action) => {
	switch (action.type) {
		case pc.FETCHING_POSTS:
			return { ...state, fetching: action.boolean };

		case pc.SET_POSTS:
			return {
				...state,
				posts: { ...state.posts, [action.user_id]: action.posts },
			};

		case pc.SET_CURRENT_POST: {
			const currentPost = state.posts?.[action.user_id].find(
				(post) => post.id === action.post_id
			);
			return { ...state, currentPost };
		}

		case pc.REMOVE_USER_POST: {
			const user_posts = state.posts[action.post.userId].filter(
				(post) => post.id !== action.post.id
			);

			return {
				...state,
				posts: { ...state.posts, [action.post.userId]: user_posts },
			};
		}

		case pc.EDIT_USER_POST: {
			const post_index = state.posts[action.post.userId].findIndex(
				(post) => action.post.id === post.id
			);

			const user_posts = [...state.posts[action.post.userId]];

			user_posts[post_index] = {
				...user_posts[post_index],
				title: action.post.title,
				body: action.post.body,
			};

			return {
				...state,
				posts: { ...state.posts, [action.post.userId]: user_posts },
			};
		}

		case pc.REMOVE_USER_POSTS: {
			return {
				...state,
				posts: { ...state.posts, [action.user_id]: [] },
			};
		}

		case pc.REMOVE_CURRENT_POST:
			return { ...state, currentPost: null };

		default:
			return state;
	}
};
