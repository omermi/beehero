export const postsConstants = {
	FETCHING_POSTS: "FETCHING_POSTS",
	SET_POSTS: "SET_POSTS",
	SET_CURRENT_POST: "SET_CURRENT_POST",
	REMOVE_CURRENT_POST: "REMOVE_CURRENT_POST",
	REMOVE_USER_POST: "REMOVE_POST",
	REMOVE_USER_POSTS: "REMOVE_USER_POSTS",
	EDIT_USER_POST: "EDIT_USER_POST",
};
