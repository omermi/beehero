import { usersConstants as uc } from "./constants";

const initialState = { users: null, currentUser: null, fetching: false };

export const users = (state = initialState, action) => {
	switch (action.type) {
		case uc.FETCHING_USERS:
			return { ...state, fetching: action.boolean };

		case uc.SET_USERS: {
			const users = action.users.map((user) => ({
				id: user?.id,
				name: user?.name,
				username: user?.username,
				email: user?.email,
				company_name: user?.company?.name,
				coords: {
					lat: user?.address?.geo?.lat,
					lng: user?.address?.geo?.lng,
				},
				posts: null,
			}));
			return { ...state, users };
		}

		case uc.REMOVE_USER: {
			const users = state.users.filter((user) => user.id !== action.user_id);
			const currentUser =
				state?.currentUser?.id === action.user_id ? null : state.currentUser;

			return { ...state, users, currentUser };
		}

		case uc.SET_CURRENT_USER: {
			const currentUser = state.users.find(
				(user) => user.id === action.user_id
			);
			return { ...state, currentUser };
		}

		default:
			return state;
	}
};
