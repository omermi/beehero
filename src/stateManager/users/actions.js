import { api } from "api/config";
import { usersConstants as uc } from "./constants";
import { removeUserPosts } from "stateManager/posts/actions";

export const getUsers = () => async (dispatch) => {
	try {
		dispatch(fetchingUsers(true));
		const res = await api.get("users");
		dispatch(setUsers(res.data));
		dispatch(fetchingUsers(false));
	} catch (e) {
		dispatch(fetchingUsers(false));
	}
};

const setUsers = (users) => ({
	type: uc.SET_USERS,
	users,
});

const fetchingUsers = (boolean) => ({
	type: uc.FETCHING_USERS,
	boolean,
});

export const removeUser = (user_id) => (dispatch) => {
	dispatch(removeUserPosts(user_id));
	dispatch({ type: uc.REMOVE_USER, user_id });
};

export const setCurrentUser = (user_id) => ({
	type: uc.SET_CURRENT_USER,
	user_id,
});
