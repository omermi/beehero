# Interactive users page

This is a website app built using React, Hooks and Redux for state management.

The app is designed with [scss](https://sass-lang.com/) and using [react-leaflet](https://react-leaflet.js.org/) for map component.

The app is adapted for large screen size as well as for mobile screen size.

## Instructions

First clone this repository.

```bash
$ git clone https://omermi@bitbucket.org/omermi/beehero.git
```

Install dependencies.

```bash
$ npm install
```

Run it

```bash
$ npm start
```
